variable "aws_access_key" {
  type    = string
  default = "test"
}

variable "aws_secret_key" {
  type    = string
  default = "test"
}

variable "aws_region" {
  type    = string
  default = "us-east-1"
}

variable "use_localstack" {
  type    = bool
  default = true
}

variable "localstack_endpoint" {
  type    = string
  default = "http://localhost:4566"
}
variable "localstack_access_key" {
  description = "LocalStack access key"
  type        = string
  default     = "LKIAQAAAAAAAHFCVUJVW"
}

variable "localstack_secret_key" {
  description = "LocalStack secret key"
  type        = string
  default     = "SiQEWV/O5tlMm9EugZ7PYVtLdsHDZnf/S59DSaud"
}